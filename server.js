const _express = require('express');
const _server = _express();
const _port = 4000;
const { Client } = require('pg');


const connectionData = {
  user: 'postgres',
  host: '172.18.0.4',
  database: 'postgres',
  password: 'postgres',
  port: 5432,
}

const client = new Client(connectionData)

_server.get('/retoibm/sumar/:sumando01/:sumando02', function(request, response) {
  try{
    var _sumando01 = new Number(request.params.sumando01);
    var _sumando02 = new Number(request.params.sumando02);
    var _resultado = _sumando01 + _sumando02;
    
    if (typeof _resultado !== "undefined" && _resultado!==null && !isNaN(_resultado)){    
	client.connect();
        client.query('INSERT INTO OPERACION.SUMA(pri_sumando,seg_sumando,suma) VALUES ($1,$2,$3)',[_sumando01,_sumando02,_resultado])
//        client.end()

       return response.status(200).json({resultado : _resultado});
      
    }else{
      return response.status(400).json({resultado : "Bad Request"});
    }
  }
  catch(e){
    return response.status(500).json({resultado : e});
  }
});


_server.listen(_port, () => {
   console.log(`Server listening at ${_port}`);
});
